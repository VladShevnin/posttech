﻿using ClientData.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace ClientData.Repository
{
    public interface ISendMessageRepository
    {
        SendMessage FirstItem();
        SendMessage Create(SendMessage item);
        SendMessage Update(SendMessage item);
        void Delete(long id);
    }
}
