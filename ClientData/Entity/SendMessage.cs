﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClientData.Entity
{
    public class SendMessage
    {
        public long Id { get; set; }
        public DateTime CreateData { get; set; }
        public string Message { get; set; }
    }
}
