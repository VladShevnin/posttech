﻿using ClientData.Entity;
using Microsoft.EntityFrameworkCore;

namespace Client
{
    public class DatabaseContext : DbContext
    {
        public DbSet<SendMessage> SendMessage { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=.\;Database=PostTechClient;Trusted_Connection=True;MultipleActiveResultSets=true");
        }
    }

}
