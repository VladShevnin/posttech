﻿using ClientData.Entity;
using ClientData.Repository;
using Newtonsoft.Json;
using NLog;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TransferData;

namespace Client
{
    public class SendMessagesTask
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        ISendMessageRepository sendMesageRepository;
        Task sendMessageTask;
        WebClient client;
        string serverUrl;

        public SendMessagesTask(string url, ISendMessageRepository repository)
        {
            sendMesageRepository = repository;
            client = new WebClient();
            serverUrl = url;

            CreateSendTask();
        }

        private void CreateSendTask()
        {
            logger.Info("Create Send Task");
            sendMessageTask = Task.Factory.StartNew(() =>
            {
                SendMessage sendMessage = sendMesageRepository.FirstItem();
                while (sendMessage != null)
                {
                    try
                    {
                        string json = JsonConvert.SerializeObject(new MessagePacket() { Message = sendMessage.Message });

                        client.Headers.Add(HttpRequestHeader.ContentType, "application/json");
                        client.Headers.Add(HttpRequestHeader.Accept, "application/json");

                        logger.Info($"Отправка данных {json} на сервер {serverUrl}");
                        client.UploadString(serverUrl, json);

                        logger.Info($"Удаление данных {sendMessage.Id} из списка на отправку");
                        sendMesageRepository.Delete(sendMessage.Id);
                    }
                    catch (Exception ex)
                    {
                        logger.Error($"Ошибка при отправки данных на сервер {ex}");
                    }

                    sendMessage = sendMesageRepository.FirstItem();
                }
            });

        }

        public void AddMessage(string message)
        {
            sendMesageRepository.Create(new SendMessage() { Message = message, CreateData = DateTime.UtcNow });
            SendAll();
        }

        private void SendAll()
        {
            if (sendMessageTask.IsCompleted)
            {
                CreateSendTask();
            }
        }

    }
}
