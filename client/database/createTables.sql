if not exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SendMessage]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
CREATE TABLE [dbo].[SendMessage](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreateData] [datetime] NOT NULL,
	[Message] [nvarchar](1000) NULL
) ON [PRIMARY]
GO