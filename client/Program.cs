﻿using Client;
using Client.Repository;
using NLog;
using System;

namespace client
{
    class Program
    {
        const string Url = "https://localhost:5001/message/add";
        private static Logger logger = LogManager.GetCurrentClassLogger();
        static void Main(string[] args)
        {
            var context = new DatabaseContext();

            logger.Info("Запуск клиента");
            SendMessagesTask messages = new SendMessagesTask(Url, new SQLSendMessageRepository(context)); 

            Console.WriteLine("Введите строку для отправки");
            while (true)
            {
                string newMessage = Console.ReadLine();

                logger.Info($"Отправка сообщения: {newMessage}");
                messages.AddMessage(newMessage);
            }
        }
    }
}
