﻿using ClientData.Entity;
using ClientData.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Client.Repository
{
    public class MemorySendMessageRepository : ISendMessageRepository
    {
        Dictionary<long, SendMessage> messages = new Dictionary<long, SendMessage>();
        int curMessageId = 0;
        public SendMessage Create(SendMessage item)
        {
            item.Id = curMessageId;
            messages.Add(curMessageId, item);
            curMessageId++;

            return item;
        }

        public void Delete(long id)
        {
            messages.Remove(id);
        }

        public SendMessage FirstItem()
        {
            return messages.Values.OrderBy(i => i.CreateData).FirstOrDefault();
        }

        public SendMessage Update(SendMessage item)
        {
            messages[item.Id] = item;
            return item;
        }
    }
}