﻿using ClientData.Entity;
using ClientData.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Client.Repository
{
    public class SQLSendMessageRepository : ISendMessageRepository
    {
        private DatabaseContext db;

        public SQLSendMessageRepository(DatabaseContext db)
        {
            this.db = db;
        }
        public SendMessage Create(SendMessage item)
        {
            db.SendMessage.Add(item);
            db.SaveChanges();
            return item;
        }

        public void Delete(long id)
        {
            SendMessage item = db.SendMessage.Find(id);
            if (item != null)
                db.SendMessage.Remove(item);
            db.SaveChanges();
        }

        public SendMessage FirstItem()
        {
            return db.SendMessage.Select(item => item).OrderBy(item => item.CreateData).FirstOrDefault();
        }

        public SendMessage Update(SendMessage item)
        {
            db.SendMessage.Update(item);
            db.SaveChanges();
            return item;
        }
    }
}
