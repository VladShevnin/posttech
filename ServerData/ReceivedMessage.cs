﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServerData
{
    public class ReceivedMessage
    {
        public long Id { get; set; }
        public string Message { get; set; }
        public DateTime CreateData { get; set; }
        public string Ip { get; set; }
    }
}
