﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServerData
{
    public interface IReceivedMessageRepository
    {
        IEnumerable<ReceivedMessage> AllItems();
        ReceivedMessage Create(ReceivedMessage item);
        ReceivedMessage Update(ReceivedMessage item);
        void Delete(int id);
    }
}
