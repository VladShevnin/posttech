﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ServerData;
using System;
using TransferData;

namespace Server.Controllers
{
    [ApiController]
    [Route("message")]
    public class MessageController : ControllerBase
    {
        private readonly ILogger<MessageController> logger;
        IReceivedMessageRepository repository;
        IHttpContextAccessor accessor;

        public MessageController(IReceivedMessageRepository repository, IHttpContextAccessor accessor, ILogger<MessageController> logger)
        {
            this.repository = repository;
            this.accessor = accessor;
            this.logger = logger;
        }


        [HttpPost("add")]
        public void AddMessage([FromBody] MessagePacket message)
        {
            logger.LogInformation($"Добавление новой записи {message.Message}");
            repository.Create(new ReceivedMessage() { Message = message.Message, Ip = accessor.HttpContext.Connection.RemoteIpAddress.ToString(), CreateData = DateTime.UtcNow });
        }
    }
}
