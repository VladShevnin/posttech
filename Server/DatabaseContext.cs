﻿using Microsoft.EntityFrameworkCore;
using ServerData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext()
        {

        }

        public DatabaseContext(DbContextOptions options)
            : base(options)
        {

        }

        public DbSet<ReceivedMessage> ReceivedMessage { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=.\;Database=PostTechServer;Trusted_Connection=True;MultipleActiveResultSets=true");
        }

    }
}
