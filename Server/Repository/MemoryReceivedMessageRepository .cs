﻿using ServerData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Repository
{
    public class MemoryReceivedMessageRepository : IReceivedMessageRepository
    {
        Dictionary<long, ReceivedMessage> messages = new Dictionary<long, ReceivedMessage>();
        int curMessageId = 0;

        public IEnumerable<ReceivedMessage> AllItems()
        {
            return messages.Values;
        }

        public ReceivedMessage Create(ReceivedMessage item)
        {
            item.Id = curMessageId;
            messages.Add(curMessageId, item);
            curMessageId++;

            return item;
        }

        public void Delete(int id)
        {
            messages.Remove(id);
        }

        public ReceivedMessage Update(ReceivedMessage item)
        {
            messages[item.Id] = item;
            return item;
        }
    }
}
