﻿using Microsoft.EntityFrameworkCore;
using ServerData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Repository
{
    public class SqlReceivedMessageRepository: IReceivedMessageRepository
    {
        private DbContextOptions<DatabaseContext> dbContextOptions;
        public SqlReceivedMessageRepository(DbContextOptions<DatabaseContext> dbContextOptions)
        {
            this.dbContextOptions = dbContextOptions;
        }

        public IEnumerable<ReceivedMessage> AllItems()
        {
            using (var db = new DatabaseContext(dbContextOptions))
            {
                return db.ReceivedMessage.Select(item => item).OrderBy(item => item.CreateData).ToList();
            }
        }

        public ReceivedMessage Create(ReceivedMessage item)
        {
            using (var db = new DatabaseContext(dbContextOptions))
            {
                db.ReceivedMessage.Add(item);
                db.SaveChanges();
            }
            return item;
        }

        public void Delete(int id)
        {
            using (var db = new DatabaseContext(dbContextOptions))
            {
                ReceivedMessage item = db.ReceivedMessage.Find(id);
                if (item != null)
                    db.ReceivedMessage.Remove(item);
                db.SaveChanges();
            }
        }

        public ReceivedMessage Update(ReceivedMessage item)
        {
            using (var db = new DatabaseContext(dbContextOptions))
            {
                db.ReceivedMessage.Update(item);
                db.SaveChanges();
            }
            return item;
        }
    }
}
