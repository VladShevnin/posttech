﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using ServerData;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Server.Common
{
    public class ConcoleHostedService : IHostedService, IDisposable
    {
        private readonly ILogger<ConcoleHostedService> logger;

        private Task executingTask;
        private readonly CancellationTokenSource stoppingCts = new CancellationTokenSource();
        private IReceivedMessageRepository repository;

        public ConcoleHostedService(ILogger<ConcoleHostedService> logger, IReceivedMessageRepository repository)
        {
            this.logger = logger;
            this.repository = repository;
        }

        public Task StartAsync(CancellationToken stoppingToken)
        {
            logger.LogInformation("Timed Hosted Service running.");

            executingTask = Task.Factory.StartNew(() =>
            {
                while (!stoppingToken.IsCancellationRequested)
                {
                    string newCommand = Console.ReadLine();
                    if (newCommand != null && newCommand.ToLower().Equals("print"))
                    {
                        Console.WriteLine("Data base list");
                        foreach (var item in repository.AllItems())
                        {
                            Console.WriteLine($"{item.CreateData}   {item.Ip}  {item.Message}");
                        }
                    }
                }
            }, stoppingCts.Token);

            return Task.CompletedTask;
        }

        public virtual async Task StopAsync(CancellationToken cancellationToken)
        {
            // Stop called without start
            if (executingTask == null)
            {
                return;
            }

            stoppingCts.Cancel();
        }

        public void Dispose()
        {
            stoppingCts.Cancel();
        }
    }
}
